$(document).ready(function () {
    $("#country").change(function () {
        let country = $("#country").val();
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": jQuery('meta[name="csrf-token"]').attr(
                    "content"
                ),
            },
        });
        $.ajax({
            type: "post",
            url: "/updatestates",
            data: { country: country },
            success: function (data) {
                if (data) {
                    $("#states").html(data);
                } else {
                }
            },
            error: function (e) {
                console.log(e);
            },
        });
    });
});
