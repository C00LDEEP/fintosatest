<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('List') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{route('companies.create')}}" class="btn btn-primary">Add New Company</a>
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Website</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(isset($companies))
                    @foreach($companies as $company)
                        <tr>
                        <td>{{$company->name}}</td>
                        <td>{{$company->email}}</td>
                        <td>{{$company->website}}</td> 
                        <td><a href="{{route('companies.edit',$company->id)}}" class="btn btn-primary">Edit</a></td>
                        <td><form action="{{route('companies.destroy',$company->id)}}" method="post">
                        <button type="submit" class="btn btn-danger">Delete</button>
                            @csrf @method("delete")
                        </form></td>
                        </tr>
                    @endforeach
                    @endif                                           
                    </tbody>
                </table>
                   {{ $companies->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
