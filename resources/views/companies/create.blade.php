<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create New Company') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                @if($errors->any())
                <div class="alert-danger py-4">
                    <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{route('companies.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                <div class="form-group">
                    <label for="exampleInputName">Name</label>
                    <input type="text" name="name" class="form-control" id="exampleInputName" placeholder="Enter Name" value="{{old('name')}}">
                </div>

                <div class="form-group">
                    <label for="exampleInputName">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputName" aria-describedby="emailHelp" placeholder="Enter Email" value="{{old('email')}}">
                </div>

                <div class="form-group">
                    <label for="exampleFormControlFile1">Logo</label>
                    <input type="file" name="logo" class="form-control-file" id="exampleFormControlFile1">
                </div>     
                
                <div class="form-group">
                    <label for="exampleInputName">Website</label>
                    <input type="text" name="website" class="form-control" id="exampleInputWebsite" placeholder="Enter Website" value="{{old('website')}}">
                </div>

                <div class="form-group">
                    <label for="exampleInputName">Address</label>
                    <input type="text" name="address" class="form-control" id="exampleInputAddress" aria-describedby="emailHelp" placeholder="Enter Address" value="{{old('Address')}}">
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Country</label>
                    <select name="country" class="form-control" id="country">
                        <option value="">Select</option>
                    @foreach($countries as $country)
                    <option value="{{$country->id}}">{{$country->name}}</option>
                    @endforeach
                    </select>
                </div>

                <div id="states">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">State</label>
                        <select name="state" class="form-control" id="exampleFormControlSelect1">
                        <option value="">Select</option>                        
                        </select>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
