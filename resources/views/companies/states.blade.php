<div class="form-group">
    <label for="exampleFormControlSelect1">State</label>
    <select name="state" class="form-control" id="exampleFormControlSelect1">
        <option value="">Select States</option>
    @foreach($states as $state)
    <option value="{{$state->id}}">{{$state->name}}</option>
    @endforeach
    </select>
</div>