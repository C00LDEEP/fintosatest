<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Hash;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRecord = [
            ["id" => '1',"name" => "admin" , "email" => "admin@admin.com" , "password" => Hash::make("password") ],
        ]; 
        User::insert($userRecord);
    }
}
