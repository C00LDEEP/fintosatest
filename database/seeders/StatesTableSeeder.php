<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\State;
class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            ["id"=>"1","country_id"=>"1","name" => "Gujarat"],
            ["id"=>"2","country_id"=>"1","name" => "Rajasthan"],
            ["id"=>"3","country_id"=>"1","name" => "Maharastra"],
            ["id"=>"4","country_id"=>"2","name"=>"DC"],
            ["id"=>"5","country_id"=>"2","name"=>"Ohio"],
            ["id"=>"6","country_id"=>"2","name"=>"Taxes"],
        ];

        State::insert($records);
    }
}
