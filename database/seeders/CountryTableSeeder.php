<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;
class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            ["id"=>"1","name"=>"India"],
            ["id"=>"2","name"=>"US"],
        ];

        Country::insert($records);
    }
}
