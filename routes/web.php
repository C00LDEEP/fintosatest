<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\EmployeesController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => ["auth"]],function(){

    Route::post("/updatestates",[CompaniesController::class, 'updateStates'])->name("updateStates");
    Route::get("/deleteImage/{id}",[CompaniesController::class, 'deleteImage'])->name("deleteImage");
    Route::resources([
        'companies' => CompaniesController::class,
        'employees' => EmployeesController::class,
    ]);
});
require __DIR__.'/auth.php';
