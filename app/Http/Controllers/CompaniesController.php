<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Country;
use App\Models\State;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(10);
        return view("companies.index",compact("companies"));
        
    }

    public function updateStates(Request $request){
        $data = $request->all();
        if(isset($data["country"])){
            $states = State::where("country_id",$data["country"])->get();
            return view("companies.states",compact('states'));
        }
        return false;
    }


    public function deleteImage($id){
        $company=Company::find($id);
        if(file_exists("storage/".$company->logo)){
            unlink("storage"."/".$company->logo);
            $company->logo = "";
            $company->update();
        }
        return redirect()->back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view("companies.create",compact("countries"));        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
        $data = $request->all();
        $name="";
        $rules =[
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email',                        
            'logo' => 'image'
        ];

        $error_msg=[
            'name.required' => 'Company name is required',
            'name.regex'=>'Please enter valid company name',
            'url.required' => 'Enter valid URL ',
            'logo.image'=>'Upload Valid Image',
        ];

        $this->validate($request,$rules,$error_msg);

        if($id==null){
            if($request->hasFile("logo") ){
                $tmp_image = $request->file("logo");
                $name = $tmp_image->getClientOriginalName();
                $request->logo->storeAs('public', $name);                
                //print "<pre>"; print_r($data); die;
            }
            Company::create(["name"=>$data["name"], "email"=>$data["email"], "logo" => $name, 
            "website"=>$data["website"], "address" => $data["address"], "country_id"=>$data["country"], "state_id" =>$data["state"] ]) ;           
        }
       
        //
        return redirect()->route("companies.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company=Company::find($id);
        $countries = Country::all();
        $states = State::where('country_id',$company->country_id)->get();
        return view("companies.edit",compact("countries","company","states"));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $company = Company::findOrfail($id);
            if($company)
            {
                if($request->hasFile("logo")){
                    $tmp_image = $request->file("logo");
                    $name = $tmp_image->getClientOriginalName();
                    $request->logo->storeAs('public', $name);   
                    $company->logo=$name;             
                }
                $company->name=$data["name"];
                $company->email=$data["email"];
                $company->website=$data["website"];
                $company->address=$data["address"];
                $company->country_id=$data["country"];
                $company->state_id=$data["state"];
                $company->update();
            }

        return redirect()->route("companies.index");              
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::findOrfail($id);
        if($company){
            $company->delete();
        }
        return redirect()->route("companies.index");                          
    }
}
